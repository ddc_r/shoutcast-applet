A simple Shoutcast player applet
================================

Installation
------------

1.  Download the archive
2.  Extract its components into your webserver root folder (or in any other folder your webserver can access, just update the corresponding entry in your HTML markup, as described below)
3.  Make sure those files can be downloaded by your visitors, on demand
4.  Create a webpage on your webserver that includes an appropriate HTML *APPLET* markup, as shown below

Configuration
-------------

-   Into your webpage add the following markup:

<!-- -->

    <applet archive="shoutcastplayerapplet2.jar" code="biz.ddcr.shampoo.applet.player.ShoutcastPlayerApplet" width="288" height="26">
        <param name="endpoint" value="http://overfitted.ddcr.biz/radio.php?.pls"/>
        Please <a href="http://java.com/">install Java</a> to tune into the radio from within this page
    </applet>

-   Change the **archive** element value with the relative path to your JAR file from the web page embedding this code, if applicable
-   Replace the value associated with the **endpoint** attribute with the full pathname or URL to the stream or file you want to play
-   Done

### Change the buttons

-   The following parameters (*PARAM* elements) can be added and point to a relative pathname (e.g. 'resources/stop.gif') or to a distant URL of a picture file: **playingImage**, **playingImageOver**, **stoppedImage**, **stoppedImageOver**, **bufferingImage** and **bufferingImageOver**
-   Adapt the applet **width** and **height** values to your picture dimensions

### Change the background colour

-   Add a colour name (e.g. 'white', 'black') or an HTML RGB value (e.g. '\#000000') to the **backgroundColour** parameter element

Demo
----

[See here]

History
-------

### v1

-   Plays Icecast, Shoutcast endpoints
-   Reads M3U
-   Supports MP3
-   Java 5 and 6-friendly

### v2

-   New Scala codebase
-   Customizable buttons without rebuilding
-   Reads M3U and PLS
-   Plays Icecast, Shoutcast, and non-streaming endpoints
-   Supports MP3, Flac, Ogg Vorbis, AAC(+) (currently broken)
-   Java 5 to 7-friendly

### v2.1

-   Updated security packaging for JRE 7u25 compatibility

Known issues
------------

Please file bug reports or submit patches to the parent project JIRA [there]

-   AAC(+) playback is currently broken
-   MP3s with large headers (e.g. containing a rather big embedded cover art picture) at the start of the file will not play

Building from source
--------------------

1.  Install Oracle or OpenJDK 6 or 7, Scala 2.9.x, Netbeans, and the experimental [Scala plugin for Netbeans]
2.  Load the provided NetBeans project file
3.  JARs must be signed, update the corresponding portion in the build.xml file with your own keys
4.  Rebuild the project

  [See here]: http://overfitted.ddcr.biz/
  [there]: http://java.net/jira/browse/SHAMPOO
  [Scala plugin for Netbeans]: http://wiki.netbeans.org/Scala